import firebase from "firebase/app"
import "firebase/analytics"
import "firebase/auth"
import "firebase/firestore"

import {firebase as firebaseConfig} from './config'

const App = firebase.apps.length ? firebase.app() : firebase.initializeApp(firebaseConfig)
const Analytics = firebase.analytics()
const DataBase = firebase.firestore()

export default App
export {DataBase, Analytics}