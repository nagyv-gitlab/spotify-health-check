import React from 'react';
import Head from 'next/head'
import Grid from '@material-ui/core/Grid';
import { Card, CardContent, CardHeader, Container, createStyles, makeStyles, Theme, Typography } from '@material-ui/core';
import NewCheck from '../Components/NewCheck';
import { Analytics } from '../firebase';

const useStyles = makeStyles((theme: Theme) =>
createStyles({
  footer: {
    position: 'fixed',
    bottom: '0px',
    paddingBottom: '10px',
  },
}),
);

export default function Home() {
  const classes = useStyles()

  Analytics.logEvent('page_view', {
    page_title: 'Home'
  })

  return (
    <Container>
      <Head>
        <title>Spotify Health Checks</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Typography variant="h1">Spotify Health Checks</Typography>
      <Grid container>

        <Grid item xs={6}>
          <Typography variant="h2">What is this</Typography>
          <Typography variant="body1">A workshop & visualization technique for helping squads/teams improve</Typography>
          <Typography variant="h2">Who is it for?</Typography>
          <ul>
            <li><Typography variant="body1">The squad/team itself</Typography></li>
            <li><Typography variant="body1">People supporting the squad</Typography></li>
          </ul>
          <Typography variant="h2">More info</Typography>
          <Typography variant="body1">For more info read <a href="https://engineering.atspotify.com/2014/09/16/squad-health-check-model/">the original article</a></Typography>.
        </Grid>

        <Grid item xs={6}>
          <Card>
            <CardHeader title="Start a new health check" />
            <CardContent>
              <ol>
                <li><Typography variant="body1">Give a name to this health check</Typography></li>
                <li><Typography variant="body1">Share the received link with squad members</Typography></li>
                <li><Typography variant="body1">Wait for feedback to be provided</Typography></li>
                <li><Typography variant="body1">Organize a meeting to discuss results</Typography></li>
                <li><Typography variant="body1">Iterate and follow progress by comparing with previous workshop results</Typography></li>
              </ol>
              <NewCheck />
            </CardContent>
          </Card>
        </Grid>
      </Grid>
      <footer className={classes.footer}>
        <hr />
        &copy; Viktor Nagy | Health check model: Henrik Kniberg & Kris1an Lindwall, with help from the other agile coaches at Spo1fy | Graphical design of cards: Mar1n Österberg
      </footer>
    </Container>
  )
}
