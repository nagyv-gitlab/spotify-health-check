import React from "react"
import { MuiPickersUtilsProvider } from "@material-ui/pickers"
import DayJSUtils from '@date-io/dayjs'
import { CssBaseline } from "@material-ui/core"

function MyApp({ Component, pageProps }) {
  return <MuiPickersUtilsProvider utils={DayJSUtils}>
    <CssBaseline />
    <Component {...pageProps} />
  </MuiPickersUtilsProvider>
}

export default MyApp
